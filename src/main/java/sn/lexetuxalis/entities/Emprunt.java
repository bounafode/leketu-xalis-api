package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@ToString
public class Emprunt {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(name = "mois_hebdo")
	private String moisHebdo;
	@Column(name = "taux_applique")
	private Double tauxApplique;
	@Column(name = "montant_emprunt")
	private Double montantEmprunt;
	@Column(name = "date_emprunt")
	private Date dateEmprunt;
	@Column(name = "date_retour", nullable = true)
	private Date dateRetour;
	@Column(name = "date_retour_reelle", nullable = true)
	private Date dateRetourReelle;
	@ManyToOne(optional = false)
	@JoinColumn(name = "membre_id", referencedColumnName = "id")
	private Membre membre;
	@Column(name = "est_paye")
	private boolean estPaye = false;
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;

}
