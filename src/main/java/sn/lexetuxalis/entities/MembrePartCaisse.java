package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity 
@Data @NoArgsConstructor @ToString
@Table(name = "membre_part_caisse")
public class MembrePartCaisse {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "mois_hebdo")
	private String moisHebdo;
	
	private Integer part;
	
	@Column(name = "date_action", nullable = true)
	private Date dateAction;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "caisse_params_id", referencedColumnName = "id")
	private CaisseParams currentCaisse;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "membre_id", referencedColumnName = "id")
	private Membre membre;

	@Column(name = "est_cloture")
	private boolean estCloture = false;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;
	
	
}
