package sn.lexetuxalis.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity 
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class Membre {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "nom_complet")
	private String nomComplet;
	
	private Integer numero;
	private Integer tel;	
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "caisse_id", referencedColumnName = "id")
	private Caisse caisse;
	
	
}
