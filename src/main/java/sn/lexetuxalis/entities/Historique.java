package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data @NoArgsConstructor @ToString
public class Historique {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String fonction;
	private String commentaire;
	
	@Column(name = "date_action")
	private Date dateAction;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "caisse_id", referencedColumnName = "id")
	private Caisse caisse;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;
}
