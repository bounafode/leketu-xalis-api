package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity 
@Data @NoArgsConstructor @ToString
public class Allocation {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "date_allocation")
	private Date dateAllocation;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "evenement_id", referencedColumnName = "id")
	private Evenement evenement;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "caisse_solidarite_id", referencedColumnName = "id")
	private CaisseSolidarite currentCaisseSolidarite;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "membre_id", referencedColumnName = "id")
	private Membre membre;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;
	
	
}
