package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@ToString
public class Penalite {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "mois_hebdo")
	private String moisHebdo;
	
	@Column(name = "montant_amende")
	private Integer montantAmende;
	
	private String commentaire;
	
	@Column(name = "date_action", nullable = true)
	private Date dateAction;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "membre_id", referencedColumnName = "id")
	private Membre membre;
	
	@Column(name = "est_paye")
	private boolean estPaye = false;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;

}
