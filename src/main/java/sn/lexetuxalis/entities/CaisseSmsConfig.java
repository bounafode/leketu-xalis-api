package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity 
@Data @NoArgsConstructor @AllArgsConstructor @ToString
@Table(name = "caisse_sms_config")
public class CaisseSmsConfig {
	@Id 
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "caisse_id", referencedColumnName = "id")
	private Caisse caisse;
	
	@Column(name = "is_admin_activated_notify_sms")
	private boolean isAdminActivatedNotifySms = false;
	
	@Column(name = "is_gestionnary_authorize_manage_caisse_sms_notifcation")
	private boolean isGestionnaryAuthorizeManageCaisseSmsNotifcation;
	
	@Column(name = "notifyMember_when_epargne")
	private boolean notifyMemberWhenEpargne = false;
	
	@Column(name = "notify_member_when_solidarite")
	private boolean notifyMemberWhenSolidarite = false;
	
	@Column(name = "notify_member_when_amende")
	private boolean notifyMemberWhenAmende = false;
	
	@Column(name = "notify_member_when_emprunt")
	private boolean notifyMemberWhenEmprunt = false;
	
	@Column(name = "notify_members_when_allocate")
	private boolean notifyMembersWhenAllocate = false;
	
	@Column(name = "notify_when_lock_unlock_account")
	private boolean notifyWhenLockUnlockAccount = false;
	
	@Column(name = "notify_when_create_account")
	private boolean notifyWhenCreateAccount = false;
	
	@Column(name = "notify_members_when_mois_hebdo_closed")
	private boolean notifyMembersWhenMoisHebdoClosed = false;
	
	@Column(name = "is_supervisor_authorize_to_manage_him_notification")
	private boolean isSupervisorAuthorizeToManageHimNotification = false;
	
	@Column(name = "notify_supervisor_close_moishebdo")
	private boolean notifySupervisorCloseMoishebdo = false;
	
	@Column(name = "notify_supervisor_close_caisse")
	private boolean notifySupervisorCloseCaisse = false;
	
	@Column(name = "notify_supervisor_change_taux_delai")
	private boolean notifySupervisorChangeTauxDelai = false;
	
	@Column(name = "create_at")
	private Date createAt;
	
	@Column(name = "update_at", nullable = true)
	private Date updateAt;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;
	
	
}
