package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import sn.lexetuxalis.constant.TYPECAISSE;

@Entity
@Data @NoArgsConstructor @AllArgsConstructor @ToString
@Table(name = "caisse_params")
public class CaisseParams {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(name = "caisse_numero")
	private String caisseNumero;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "caisse_id", referencedColumnName = "id")
	private Caisse caisse;
	
	@Column(name = "part_caisse")
	private Integer partCaisse;
	
	@Column(name = "date_debut_epargne")
	private Date dateDebutEpargne;
	
	@Column(name = "date_fin_epargne")
	private Date dateFinEpargne;
	
	private TYPECAISSE status;
	
	@Column(name = "taux_remboursement")
	private Double tauxRemboursement;
	
	@Column(name = "delai_remboursement")
	private Integer delaiRemboursement;
	
	@Column(name = "max_part_emprunt_autorise")
	private Integer maxPartEmpruntAutorise;
	
	@Column(name = "is_cloture")
	private boolean isCloture = false;
	
	@Column(name = "create_at")
	private Date createAt;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;
	
}
