package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data @NoArgsConstructor @ToString
public class CustumSms {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "caisse_id", referencedColumnName = "id")
	private Caisse caisse;
	
	private String action;
	
	@Column(name = "message_txt")
	private String messageTxt;
	
	@Column(name = "create_at")
	private Date createAt;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
}
