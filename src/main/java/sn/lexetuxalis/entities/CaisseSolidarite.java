package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity 
@Data @NoArgsConstructor @AllArgsConstructor @ToString
public class CaisseSolidarite {
	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String libelle;
	
	@Column(name = "montant_cotiser")
	private Integer montantCotiser;
	
	@Column(name = "create_at")
	private Date createAt;
	
	@Column(name = "est_cloture")
	private boolean estCloture;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "current_caisse_id", referencedColumnName = "id")
	private CaisseParams currentCaisse;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser appUser;	
}
