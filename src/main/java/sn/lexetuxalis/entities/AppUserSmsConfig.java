package sn.lexetuxalis.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Data
@NoArgsConstructor
@ToString
@Table(name = "app_user_sms_config")
public class AppUserSmsConfig {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "is_admin_activated_notify_sms")
	private boolean isAdminActivatedNotifySms = false;
	
	@Column(name = "notifity_supervisor_create_caisse")
	private boolean notifitySupervisorCreateCaisse;
	
	@ManyToOne(optional = false)
	@JoinColumn(name = "appuser_id", referencedColumnName = "id")
	private AppUser userAccompte;
	
	@Column(name = "create_at")
	private Date createAt;
	
	@Column(name = "update_at", nullable = true)
	private Date updateAt;

}
