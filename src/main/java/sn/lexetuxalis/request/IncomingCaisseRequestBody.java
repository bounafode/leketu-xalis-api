package sn.lexetuxalis.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import sn.lexetuxalis.entities.Caisse;
import sn.lexetuxalis.entities.CaisseParams;
import sn.lexetuxalis.entities.CaisseSolidarite;

@Data
@NoArgsConstructor @AllArgsConstructor
@ToString
public class IncomingCaisseRequestBody {
	private Caisse caisse;
	private CaisseSolidarite caisseSolidarite;
	private CaisseParams caisseParams;
}
