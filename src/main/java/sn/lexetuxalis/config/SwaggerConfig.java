package sn.lexetuxalis.config;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
//import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
//import static springfox.documentation.builders.PathSelectors.regex;


@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket cogeaApi() {
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("sn.lexetuxalis.web"))
//				.paths(regex("/api.*")).build()
				.paths(PathSelectors.any()).build()
				.apiInfo(metaInfo())
				.securitySchemes(Arrays.asList(apiKey()));
	}

	private ApiInfo metaInfo() {
		ApiInfo apiInfo = new ApiInfo("API REST LEKETUXAALISS", "Spring Boot Swagger BOUNA DRAME", "1.0",
				"Terms of Service", new Contact("Dmtech IT", "https://www.siitav.net", "bounafode@gmail.com"),
				"Apaceh License Version 2.0", "https://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
		return apiInfo;
	}

	private ApiKey apiKey() {
		return new ApiKey("Bearer", "Authorization", "header");
	}
}
