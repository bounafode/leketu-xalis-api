package sn.lexetuxalis.config;

public class SecurityParams {
	public static final long ACCESS_TOKEN_VALIDITY_SECONDS = 10*24*3600*1000;
    public static final String SIGNING_KEY = "bounafode@gmail.com";
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
}
