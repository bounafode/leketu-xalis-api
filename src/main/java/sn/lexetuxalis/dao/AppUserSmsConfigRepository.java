package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.AppUserSmsConfig;

public interface AppUserSmsConfigRepository extends JpaRepository<AppUserSmsConfig, Long>{

}
