package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.CaisseAutreMontant;

public interface CaisseAutreMontantRepository extends JpaRepository<CaisseAutreMontant, Long>{

}
