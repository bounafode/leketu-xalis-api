package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.Historique;

public interface HistoriqueRepository extends JpaRepository<Historique, Long>{

}
