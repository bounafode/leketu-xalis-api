package sn.lexetuxalis.dao;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sn.lexetuxalis.entities.Membre;

public interface MembreRepository extends JpaRepository<Membre, Long>{
	@Query("SELECT M FROM Membre M WHERE M.caisse.id = :id")
	public Page<Membre> membersOfCaisse(@Param("id") Long id, Pageable pageable);
}
