package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sn.lexetuxalis.entities.AppUser;

public interface AppUserRepository extends JpaRepository<AppUser, Long>{
	@Query("SELECT U FROM AppUser U WHERE U.username = :username")
	public AppUser findByUsername(@Param("username") String username);
}
