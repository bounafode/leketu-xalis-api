package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.CaisseSmsConfig;

public interface CaisseSmsConfigRepository extends JpaRepository<CaisseSmsConfig, Long>{

}
