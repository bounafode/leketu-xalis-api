package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import sn.lexetuxalis.entities.AppRole;

public interface AppRoleRepository extends JpaRepository<AppRole, Long>{
	@Query("SELECT R FROM AppRole R WHERE R.roleName = :roleName")
	public AppRole findByRoleName(@Param("roleName") String roleName);
}
