package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.CustumSms;

public interface CustumSmsRepository extends JpaRepository<CustumSms, Long>{

}
