package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.Evenement;

public interface EvenementRepository extends JpaRepository<Evenement, Long>{

}
