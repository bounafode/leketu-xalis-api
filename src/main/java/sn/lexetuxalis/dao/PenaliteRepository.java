package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.Penalite;

public interface PenaliteRepository extends JpaRepository<Penalite, Long>{

}
