package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.Allocation;

public interface AllocationRepository extends JpaRepository<Allocation, Long>{

}
