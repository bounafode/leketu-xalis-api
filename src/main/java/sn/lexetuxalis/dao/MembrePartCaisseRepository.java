package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.MembrePartCaisse;

public interface MembrePartCaisseRepository extends JpaRepository<MembrePartCaisse, Long>{

}
