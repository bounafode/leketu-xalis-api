package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.Cotisation;

public interface CotisationRepository extends JpaRepository<Cotisation, Long>{

}
