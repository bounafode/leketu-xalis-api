package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.CaisseParams;

public interface CaisseParamsRepository extends JpaRepository<CaisseParams, Long>{

}
