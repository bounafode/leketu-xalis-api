package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.Caisse;

public interface CaisseRepository extends JpaRepository<Caisse, Long>{

}
