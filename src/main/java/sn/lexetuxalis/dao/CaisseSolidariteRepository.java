package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.CaisseSolidarite;

public interface CaisseSolidariteRepository extends JpaRepository<CaisseSolidarite, Long>{

}
