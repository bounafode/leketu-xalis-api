package sn.lexetuxalis.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import sn.lexetuxalis.entities.Emprunt;

public interface EmpruntRepository extends JpaRepository<Emprunt, Long>{

}
