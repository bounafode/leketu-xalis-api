package sn.lexetuxalis.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import sn.lexetuxalis.dao.AppRoleRepository;
import sn.lexetuxalis.dao.AppUserRepository;
import sn.lexetuxalis.entities.AppRole;
import sn.lexetuxalis.entities.AppUser;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
	private final Logger log = LoggerFactory.getLogger(AccountServiceImpl.class);

	@Autowired
	private AppUserRepository appUserRepository;
	@Autowired
	private AppRoleRepository appRoleRepository;

	private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@Bean
	public BCryptPasswordEncoder getBCPE() {
		this.bCryptPasswordEncoder = new BCryptPasswordEncoder();
		return this.bCryptPasswordEncoder;
	}
	
	
	@Override
	public AppUser saveUser(AppUser appUser) {
		AppUser is_userExist = appUserRepository.findByUsername(appUser.getUsername());
		if (is_userExist!=null) throw new RuntimeException("Username already exists");
		appUser.setPassword(bCryptPasswordEncoder.encode(appUser.getPassword()));
		appUserRepository.save(appUser);
		/*
		 * for(AppRole rol: appUser.getRoles()) { addRoleToUser(appUser.getUsername(),
		 * rol.getRoleName()); }
		 */
		return appUser;
	}


	@Override
	public AppRole save(AppRole appRole) {
		return appRoleRepository.save(appRole);
	}

	@Override
	public AppUser loadUserByUsername(String username) {
		try {
			return appUserRepository.findByUsername(username);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public void addRoleToUser(String username, String rolename) {
		AppUser appUser = appUserRepository.findByUsername(username);
		AppRole appRole = appRoleRepository.findByRoleName(rolename);
		appUser.getRoles().add(appRole);
	}


	@Override
	@Transactional(readOnly = true)
	public Page<AppUser> findAll(Pageable pageable) {
		log.debug("Request to get all users");
		Page<AppUser> users = appUserRepository.findAll(pageable);
		return users;
	}

}
