package sn.lexetuxalis.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import sn.lexetuxalis.entities.AppRole;
import sn.lexetuxalis.entities.AppUser;

public interface AccountService {
	Page<AppUser> findAll(Pageable pageable);

	public AppUser saveUser(AppUser appUser);

	public AppRole save(AppRole appRole);

	public AppUser loadUserByUsername(String username);

	public void addRoleToUser(String username, String rolename);
}
