package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.AppRoleRepository;
import sn.lexetuxalis.entities.AppRole;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/roles")
@Api(value = "Role Endpoint Rest")
public class RoleResource {
	private final Logger log = LoggerFactory.getLogger(RoleResource.class);
    private static final String ENTITY_NAME = "app_role";
    @Autowired
    private AppRoleRepository appRoleRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<AppRole>> findAllRole(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Agent");
		Page<AppRole> page = appRoleRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/roles/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<AppRole> createRole(@RequestBody AppRole appRole) throws URISyntaxException{
    	log.debug("REST request to save Agent {} ", appRole);  
    	appRoleRepository.save(appRole);
    	return ResponseEntity.created(new URI("/roles/save/" +appRole.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, appRole.getId().toString()))
				.body(appRole);
    }
    
    @PutMapping("/update")
    public ResponseEntity<AppRole> updateRole(@RequestBody AppRole appRole) throws URISyntaxException {
        log.debug("REST request to update Role : {}", appRole);
        if (appRole.getId() == null) {
            return createRole(appRole);
        }
        AppRole result = appRoleRepository.save(appRole);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, appRole.getId().toString()))
            .body(result);
    }
    
    @GetMapping("/find/{id}")
	public Optional<AppRole> find(@PathVariable("id") Long id) {
    	log.debug("REST request to find Role by id {} ", id);
		try {
			return appRoleRepository.findById(id);
		} catch (Exception e) {
			return null;
		}
	}
    
    @PostMapping("/delete")
	public ResponseEntity<Void> delete(@RequestBody AppRole appRole) {
		log.debug("REST request to delete Role: {}", appRole);
		appRoleRepository.delete(appRole);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, appRole.getId().toString())).build();
	}


}
