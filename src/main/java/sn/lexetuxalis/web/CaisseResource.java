package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.constant.TYPECAISSE;
import sn.lexetuxalis.dao.CaisseParamsRepository;
import sn.lexetuxalis.dao.CaisseRepository;
import sn.lexetuxalis.dao.CaisseSolidariteRepository;
import sn.lexetuxalis.dao.HistoriqueRepository;
import sn.lexetuxalis.dao.MembrePartCaisseRepository;
import sn.lexetuxalis.dao.MembreRepository;
import sn.lexetuxalis.entities.Caisse;
import sn.lexetuxalis.entities.CaisseSmsConfig;
import sn.lexetuxalis.entities.Historique;
import sn.lexetuxalis.entities.Membre;
import sn.lexetuxalis.entities.MembrePartCaisse;
import sn.lexetuxalis.request.IncomingCaisseRequestBody;
import sn.lexetuxalis.util.CalculateDate;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/caisses")
@Api(value = "Caisse Endpoint Rest")
public class CaisseResource {
	private final Logger log = LoggerFactory.getLogger(CaisseResource.class);
    private static final String ENTITY_NAME = "caisse";
    @Autowired
    private CaisseRepository caisseRepository;
    @Autowired
    private CaisseSolidariteRepository caisseSolidariteRepository;
    @Autowired
    private CaisseParamsRepository caisseParamsRepository;
    @Autowired
    private MembreRepository membreRepository;
    @Autowired
    private MembrePartCaisseRepository membrePartCaisseRepository;
    @Autowired
    private HistoriqueRepository historiqueRepository;
    int numplus;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Caisse>> findAllCaisse(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Caisse");
		Page<Caisse> page = caisseRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/caisses/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
	@PostMapping("/save")
    public ResponseEntity<Caisse> createCaisse(@RequestBody IncomingCaisseRequestBody inco) throws URISyntaxException{
    	log.debug("REST request to save Caisse {} ", inco.getCaisse());  
    	Date dt = new Date();
    	
    	inco.getCaisse().setCreateAt(dt);
    	caisseRepository.save(inco.getCaisse());
    	
    	inco.getCaisseParams().setCreateAt(dt);
    	caisseParamsRepository.save(inco.getCaisseParams());
    	
    	inco.getCaisseSolidarite().setCreateAt(dt);
    	caisseSolidariteRepository.save(inco.getCaisseSolidarite());
    	
    	CaisseSmsConfig smsConfig = new CaisseSmsConfig();
    	smsConfig.setCaisse(inco.getCaisse());
    	smsConfig.setCreateAt(dt);
    	smsConfig.setAppUser(inco.getCaisse().getAppUser());
    	
    	numplus = 0;
    	Stream.of( "Présidente", "secretaire",
                   "Tresoriere", "Compteur",
			       "Compteur", "Detentrice de clef",
			       "Detentrice de clef","Detentrice de clef",
			       "Charger des amendes").forEach(profile->{
			    	   numplus++;
			    	   Membre membre=new Membre(null, profile, numplus, null, inco.getCaisse());
					membreRepository.save(membre);
			       });
    	for (int i = 1; i < 22; i++) {
    		numplus++;
			Membre membre=new Membre(null, "Membre simple", numplus, null, inco.getCaisse());
			membreRepository.save(membre);
		}
    	
    	if(inco.getCaisseParams().getStatus().equals(TYPECAISSE.mensuelle)) {
    		Page<Membre> membres = membreRepository.membersOfCaisse(inco.getCaisse().getId(), null);
    		for (Membre membre : membres) {
				MembrePartCaisse membrePartCaisse = new MembrePartCaisse();
				membrePartCaisse.setAppUser(inco.getCaisse().getAppUser());
				membrePartCaisse.setMembre(membre);
				membrePartCaisse.setCurrentCaisse(inco.getCaisseParams());
				membrePartCaisse.setDateAction(dt);
				membrePartCaisse.setPart(null);
				membrePartCaisse.setEstCloture(false);
				membrePartCaisse.setMoisHebdo(CalculateDate.formatDateToMMMMyyyy(inco.getCaisseParams().getDateDebutEpargne()));
				membrePartCaisseRepository.save(membrePartCaisse);
			}
    	}
    	
    	Historique historique=new Historique();
    	historique.setAppUser(inco.getCaisse().getAppUser());
    	historique.setCaisse(inco.getCaisse());
    	historique.setDateAction(dt);
    	historique.setFonction("insertion");
    	historique.setCommentaire("création d'une nouvelle caisse par le gestionnaire dont le login est "+inco.getCaisse().getAppUser().getUsername());
    	historiqueRepository.save(historique);
    	
    	return ResponseEntity.created(new URI("/caisses/save/" +inco.getCaisse().getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, inco.getCaisse().getId().toString()))
				.body(inco.getCaisse());
    }
    
    @PutMapping("/update")
    public ResponseEntity<Caisse> updateCaisse(@RequestBody Caisse caisse) throws URISyntaxException {
        log.debug("REST request to update Caisse : {}", caisse);
        if (caisse.getId() == null) {
            return null;
        }
        Caisse result = caisseRepository.save(caisse);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, caisse.getId().toString()))
            .body(result);
    }
    
    @GetMapping("/find/{id}")
	public Optional<Caisse> find(@PathVariable("id") Long id) {
    	log.debug("REST request to find Caisse by id {} ", id);
		try {
			return caisseRepository.findById(id);
		} catch (Exception e) {
			return null;
		}
	}
    
    @PostMapping("/delete")
	public ResponseEntity<Void> delete(@RequestBody Caisse caisse) {
		log.debug("REST request to delete Caisse: {}", caisse);
		caisseRepository.delete(caisse);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, caisse.getId().toString())).build();
	}


}
