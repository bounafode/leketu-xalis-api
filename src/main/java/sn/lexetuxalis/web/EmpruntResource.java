package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.EmpruntRepository;
import sn.lexetuxalis.entities.Emprunt;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/emprunts")
@Api(value = "Emprunt Endpoint Rest")
public class EmpruntResource {
	private final Logger log = LoggerFactory.getLogger(EmpruntResource.class);
    private static final String ENTITY_NAME = "emprunt";
    @Autowired
    private EmpruntRepository empruntRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Emprunt>> findAllEmprunt(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Emprunt");
		Page<Emprunt> page = empruntRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/emprunts/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<Emprunt> addEmprunt(@RequestBody Emprunt emprunt) throws URISyntaxException{
    	log.debug("REST request to save Emprunt {} ", emprunt);  
    	empruntRepository.save(emprunt);
    	return ResponseEntity.created(new URI("/emprunts/save/" +emprunt.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, emprunt.getId().toString()))
				.body(emprunt);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Emprunt> rembourserEmprunt(@RequestBody Emprunt emprunt) throws URISyntaxException {
        log.debug("REST request to update Emprunt : {}", emprunt);
       
        Emprunt result = empruntRepository.save(emprunt);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, emprunt.getId().toString()))
            .body(result);
    }
    
   
}
