package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.PenaliteRepository;
import sn.lexetuxalis.entities.Penalite;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/penalites")
@Api(value = "Penalite Endpoint Rest")
public class PenaliteResource {
	private final Logger log = LoggerFactory.getLogger(PenaliteResource.class);
    private static final String ENTITY_NAME = "penalite";
    @Autowired
    private PenaliteRepository penaliteRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Penalite>> findAllRole(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Penalite");
		Page<Penalite> page = penaliteRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/penalites/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<Penalite> addAmende(@RequestBody Penalite penalite) throws URISyntaxException{
    	log.debug("REST request to save Penalite {} ", penalite);  
    	penaliteRepository.save(penalite);
    	return ResponseEntity.created(new URI("/penalites/save/" +penalite.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, penalite.getId().toString()))
				.body(penalite);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Penalite> reglerAmende(@RequestBody Penalite penalite) throws URISyntaxException {
        log.debug("REST request to update Penalite : {}", penalite);
       
        Penalite result = penaliteRepository.save(penalite);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, penalite.getId().toString()))
            .body(result);
    }
    
   
}
