package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.CotisationRepository;
import sn.lexetuxalis.entities.Cotisation;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/cotisation-solidarite")
@Api(value = "Cotisation Endpoint Rest")
public class CotisationResource {
	private final Logger log = LoggerFactory.getLogger(CotisationResource.class);
    private static final String ENTITY_NAME = "cotisation";
    @Autowired
    private CotisationRepository cotisationRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Cotisation>> findAll(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Cotisation");
		Page<Cotisation> page = cotisationRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/cotisation-solidarite/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<Cotisation> saveCotisation(@RequestBody Cotisation cotisation) throws URISyntaxException{
    	log.debug("REST request to save Allocation {} ", cotisation);  
    	cotisationRepository.save(cotisation);
    	return ResponseEntity.created(new URI("/cotisation-solidarite/save/" +cotisation.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, cotisation.getId().toString()))
				.body(cotisation);
    }
    
    
    @PostMapping("/delete")
	public ResponseEntity<Void> cancelCotisation(@RequestBody Cotisation cotisation) {
		log.debug("REST request to delete Allocation: {}", cotisation);
		cotisationRepository.delete(cotisation);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, cotisation.getId().toString())).build();
	}


}
