package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.entities.AppUser;
import sn.lexetuxalis.service.AccountService;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping("/users")
public class UserResource {
	private final Logger log = LoggerFactory.getLogger(UserResource.class);

    private static final String ENTITY_NAME = "appuser";
    
	@Autowired
	private AccountService accountService;
	
	@PostMapping("/register")
	public ResponseEntity<AppUser> create(@Validated @RequestBody AppUser appUser) throws URISyntaxException{
		log.debug("REST request to save User : {}", appUser);
        if (appUser.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexist", "A id user is already exist")).body(null);
        }
		AppUser result = accountService.saveUser(appUser);
		return ResponseEntity.created(new URI("/users/save/" +result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}
	
	@PostMapping("/update")
	public ResponseEntity<AppUser> update(@Validated @RequestBody AppUser appUser) throws URISyntaxException{
		log.debug("REST request to update User : {}", appUser);
		AppUser result = accountService.saveUser(appUser);
		return ResponseEntity.created(new URI("/users/save/" +result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}
	
	@GetMapping("/find/{username}")
	public ResponseEntity<AppUser> find(@PathVariable String username) throws URISyntaxException{
		log.debug("REST request User by Username : {}", username);
		AppUser result = accountService.loadUserByUsername(username);
		return ResponseEntity.created(new URI("/users/save/" +result.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
				.body(result);
	}
	
	@GetMapping("/findAll")
    public ResponseEntity<List<AppUser>> allUsers(@ApiParam Pageable pageable)
        throws URISyntaxException {
        log.debug("REST request to get all users");
        Page<AppUser> page = accountService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/users/findAll");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }
		
}
