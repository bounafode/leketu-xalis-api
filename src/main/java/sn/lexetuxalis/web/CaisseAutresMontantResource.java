package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.CaisseAutreMontantRepository;
import sn.lexetuxalis.entities.CaisseAutreMontant;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/caisse/autre-montant")
@Api(value = "Caisse autre montant Endpoint Rest")
public class CaisseAutresMontantResource {
	private final Logger log = LoggerFactory.getLogger(CaisseAutresMontantResource.class);
    private static final String ENTITY_NAME = "caisse_autre_montant";
    @Autowired
    private CaisseAutreMontantRepository caisseAutreMontantRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<CaisseAutreMontant>> findAllCaisseAutreMontant(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Caisse");
		Page<CaisseAutreMontant> page = caisseAutreMontantRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/caisse/autre-montant/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<CaisseAutreMontant> defineMontant(@RequestBody CaisseAutreMontant autreMontant) throws URISyntaxException{
    	log.debug("REST request to save CaisseAutreMontant {} ", autreMontant);  
    	caisseAutreMontantRepository.save(autreMontant);
    	return ResponseEntity.created(new URI("/caisse/autre-montant/save/" +autreMontant.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, autreMontant.getId().toString()))
				.body(autreMontant);
    }
    

}
