package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.HistoriqueRepository;
import sn.lexetuxalis.entities.Historique;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/historiques")
@Api(value = "Allocation Endpoint Rest")
public class HistoriqueResource {
	private final Logger log = LoggerFactory.getLogger(HistoriqueResource.class);
    private static final String ENTITY_NAME = "historique";
    @Autowired
    private HistoriqueRepository historiqueRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Historique>> findAll(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Historique");
		Page<Historique> page = historiqueRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/historiques/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<Historique> create(@RequestBody Historique historique) throws URISyntaxException{
    	log.debug("REST request to save Historique {} ", historique);  
    	historiqueRepository.save(historique);
    	return ResponseEntity.created(new URI("/historiques/save/" +historique.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, historique.getId().toString()))
				.body(historique);
    }
    
    
    @PostMapping("/delete")
	public ResponseEntity<Void> delete(@RequestBody Historique historique) {
		log.debug("REST request to delete Historique: {}", historique);
		historiqueRepository.delete(historique);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, historique.getId().toString())).build();
	}


}
