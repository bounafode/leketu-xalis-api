package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.CaisseParamsRepository;
import sn.lexetuxalis.entities.CaisseParams;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/caisse-params")
@Api(value = "Caisse Endpoint Rest")
public class CaisseParamsResource {
	private final Logger log = LoggerFactory.getLogger(CaisseParamsResource.class);
    private static final String ENTITY_NAME = "caisse_params";
    @Autowired
    private CaisseParamsRepository caisseParamsRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<CaisseParams>> findAllCaisseParams(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Caisse");
		Page<CaisseParams> page = caisseParamsRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/caisse-params/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<CaisseParams> createCaisseParametres(@RequestBody CaisseParams caisseParams) throws URISyntaxException{
    	log.debug("REST request to save CaisseParams {} ", caisseParams);  
    	caisseParamsRepository.save(caisseParams);
    	return ResponseEntity.created(new URI("/caisse-params/save/" +caisseParams.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, caisseParams.getId().toString()))
				.body(caisseParams);
    }
    
    @PutMapping("/update")
    public ResponseEntity<CaisseParams> update(@RequestBody CaisseParams caisseParams) throws URISyntaxException {
        log.debug("REST request to update Caisse : {}", caisseParams);
        CaisseParams result = caisseParamsRepository.save(caisseParams);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, caisseParams.getId().toString()))
            .body(result);
    }


}
