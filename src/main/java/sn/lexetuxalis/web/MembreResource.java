package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.MembreRepository;
import sn.lexetuxalis.entities.Membre;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/membres")
@Api(value = "Membre Endpoint Rest")
public class MembreResource {
	private final Logger log = LoggerFactory.getLogger(MembreResource.class);
    private static final String ENTITY_NAME = "membre";
    @Autowired
    private MembreRepository membreRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Membre>> findAll(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Membre");
		Page<Membre> page = membreRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/membres/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @GetMapping("/findMembersOfCaisse")
	public ResponseEntity<List<Membre>> findMembersOfCaisse(@PathVariable("id") Long id, @ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Membre of caisse");
		Page<Membre> page = membreRepository.membersOfCaisse(id, pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/membres/findMembersOfCaisse");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<Membre> ajouterMembre(@RequestBody Membre membre) throws URISyntaxException{
    	log.debug("REST request to save Membre {} ", membre);  
    	membreRepository.save(membre);
    	return ResponseEntity.created(new URI("/membres/save/" +membre.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, membre.getId().toString()))
				.body(membre);
    }
    
    @PutMapping("/update")
    public ResponseEntity<Membre> update(@RequestBody Membre membre) throws URISyntaxException {
        log.debug("REST request to update Membre : {}", membre);
       
        Membre result = membreRepository.save(membre);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, membre.getId().toString()))
            .body(result);
    }
    
   
}
