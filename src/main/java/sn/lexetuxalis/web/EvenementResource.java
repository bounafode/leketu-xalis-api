package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.EvenementRepository;
import sn.lexetuxalis.entities.Evenement;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/evenements")
@Api(value = "Evenement Endpoint Rest")
public class EvenementResource {
	private final Logger log = LoggerFactory.getLogger(EvenementResource.class);
    private static final String ENTITY_NAME = "evenement";
    @Autowired
    private EvenementRepository evenementRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Evenement>> findAllAllocation(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Evenement");
		Page<Evenement> page = evenementRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/evenements/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<Evenement> create(@RequestBody Evenement evenement) throws URISyntaxException{
    	log.debug("REST request to save Evenement {} ", evenement);  
    	evenementRepository.save(evenement);
    	return ResponseEntity.created(new URI("/evenements/save/" +evenement.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, evenement.getId().toString()))
				.body(evenement);
    }
    


}
