package sn.lexetuxalis.web;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import sn.lexetuxalis.dao.AllocationRepository;
import sn.lexetuxalis.entities.Allocation;
import sn.lexetuxalis.util.HeaderUtil;
import sn.lexetuxalis.util.PaginationUtil;

@RestController
@RequestMapping(value = "/allocations")
@Api(value = "Allocation Endpoint Rest")
public class AllocationResource {
	private final Logger log = LoggerFactory.getLogger(AllocationResource.class);
    private static final String ENTITY_NAME = "allocation";
    @Autowired
    private AllocationRepository allocationRepository;
    
    @GetMapping("/findAll")
	public ResponseEntity<List<Allocation>> findAllAllocation(@ApiParam Pageable pageable) throws URISyntaxException {
		log.debug("REST request find all Allocation");
		Page<Allocation> page = allocationRepository.findAll(pageable);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/allocations/findAll");
		return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
	}
    
    @PostMapping("/save")
    public ResponseEntity<Allocation> create(@RequestBody Allocation alloc) throws URISyntaxException{
    	log.debug("REST request to save Allocation {} ", alloc);  
    	allocationRepository.save(alloc);
    	return ResponseEntity.created(new URI("/allocations/save/" +alloc.getId()))
				.headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, alloc.getId().toString()))
				.body(alloc);
    }
    
    
    @PostMapping("/delete")
	public ResponseEntity<Void> delete(@RequestBody Allocation allocation) {
		log.debug("REST request to delete Allocation: {}", allocation);
		allocationRepository.delete(allocation);
		return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, allocation.getId().toString())).build();
	}


}
