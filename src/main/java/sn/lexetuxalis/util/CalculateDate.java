package sn.lexetuxalis.util;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public final class CalculateDate {

	public static String formatDateToMMMMyyyy(Date date) {
		// number or index of month
	    LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
	    int monthNumero = localDate.getMonthValue() - 1;
	    int annee = localDate.getYear();
	    
	    // number or index of month
	    String[] shortMonths = new DateFormatSymbols().getShortMonths();
	    String fmrtToFrench = shortMonths[monthNumero].toString();
	    String subsdate = fmrtToFrench.substring(0, fmrtToFrench.length() - 1);
	    // UpperCase first letter
	    String subsdateUpper = subsdate.substring(0,1).toUpperCase() + subsdate.substring(1).toLowerCase();
	    // format MMMM-yyyy
	    String finalResul = subsdateUpper+"-"+annee;
	    
	    return finalResul;
	}
	
	public static boolean isPermittedToCreateNewCaisse(Date lastMonthOfCaisse) {
		// le dernier jour du mois de la caisse en cours
		boolean isLastMonthOfCurentCaisseFinish;
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String stringDate = df.format(lastMonthOfCaisse);
		DateTimeFormatter dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.US); 
		LocalDate date = LocalDate.parse(stringDate, dateFormat);
		LocalDate lastDateOfMonth = date.withDayOfMonth(date.getMonth().length(date.isLeapYear()));
		
		// date courante
		Date dateToConvert = new Date();
		LocalDate currentLocateDate = dateToConvert.toInstant()
	      .atZone(ZoneId.systemDefault())
	      .toLocalDate();
		
		System.out.println("fin du mois "+lastDateOfMonth);
		System.out.println("currentLocateDate "+currentLocateDate);
		boolean isPassed = lastDateOfMonth.isAfter(currentLocateDate);
		System.out.println(isPassed);
		if(isPassed) {
			isLastMonthOfCurentCaisseFinish = false;
			System.out.println("mois non terminé");
		}else {
			System.out.println("mois terminé");
			isLastMonthOfCurentCaisseFinish = true;
		}
		return isLastMonthOfCurentCaisseFinish;
	}
	
}
