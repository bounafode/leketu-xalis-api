package sn.lexetuxalis.app;

//import java.util.stream.Stream;
//
//import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.EnableMBeanExport;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jmx.support.RegistrationPolicy;

//import sn.lexetuxalis.entities.AppRole;
//import sn.lexetuxalis.entities.AppUser;
//import sn.lexetuxalis.service.AccountService;


@EnableJpaRepositories(basePackages = { "sn.lexetuxalis" })  
@EntityScan(basePackages = { "sn.lexetuxalis" })  
@ComponentScan(basePackages = { "sn.lexetuxalis" })
@EnableMBeanExport(registration = RegistrationPolicy.IGNORE_EXISTING)
@SpringBootApplication
public class LeketuXaalisApplication extends SpringBootServletInitializer implements CommandLineRunner{
//	@Autowired
//	private AccountService accountService;
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(LeketuXaalisApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(LeketuXaalisApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
//		accountService.save(new AppRole(null, "ADMIN"));
//		accountService.save(new AppRole(null, "SUPERVISEUR"));
//		accountService.save(new AppRole(null, "GESTIONNAIRE_CAISSE"));
//		accountService.save(new AppRole(null, "USER"));
//		Stream.of("superadmin").forEach(ulisateur->{
//			AppUser appUser=new AppUser();
//			appUser.setUsername(ulisateur);
//			appUser.setPassword("passer");
//			appUser.setActive(true);
//			accountService.saveUser(appUser);
//		});
//		accountService.addRoleToUser("superdamin", "ADMIN");
		System.out.println("LeketuXaaliss RESTUL app running on 8095 port...");
	}

}
